s = open("sorted.txt","r")   
r = open("final.txt", "w")   
t = open("result.txt", "w")   
thisKey = ""
thisValue = 0
maxc = 0
bcity = ""
for line in s:
    data = line.strip().split('\t')
    if len(data) != 2:  # if bad input line
       continue             # ignore it

    city, count = data  # read into variables
    if city != thisKey:
        if thisKey:
            # output the last key value pair result
            r.write(thisKey + '\t' + str(thisValue)+'\n')
            #print thisKey + '\t' + str(thisValue)+'\n'
        # start over when changing keys
        thisKey = city
        thisValue = 0
        # apply the aggregation function
    thisValue += int(count)
    if thisValue > maxc:
        bcity = thisKey
        maxc = thisValue
        
print str(maxc) +'\t' + bcity
t.write(str(maxc) +'\t' + bcity)
r.close()
s.close()

t.close()



