# Makemytrip
	## This project is a part of the course 44564-Data Intensive Systems at Northwest for Spring 2018 semester. The group 1B of that course is implementing this. This group has 4 people in total, which is a collaboration of two teams comprising of 2 members each.

## Teams Members:
	### Group1B-
		#### Team 1-4:
		1. Sangeetha Detne
		* Mail Id: s530668@nwmissouri.edu
		* Major: Applied Computer Science
		
		2. Venkata Naga Kamal Nadh Meduri
		* Mail Id: s528747@nwmissouri.edu
		* Major: Applied Computer Science
		
		#### Team 1-3:
		1. Tilak Vinuth Nag Dhulipalla
		* Mail Id: s528817@nwmissouri.edu
		* Major: Applied Computer Science
		
		2. Anil Seshu Kumar Akula
		* Mail Id: s528797@nwmissouri.edu
		* Major: Applied Computer Science

## Repository Link:
	* (https://bitbucket.org/sangeetha_detne/makemytripteam1b/src) 

## IssueTracker:
	* (https://bitbucket.org/dashboard/issues) 

## Introduction:
We are going to develop a map-reduce program to analyze the facts for make my trip  We have a dataset which includes results of international football matches starting from different who have booked using makemytrip website. The dataset is around 200064 records.
	
	* Data Source: (https://www.kaggle.com/PromptCloudHQ/hotels-on-makemytrip/data)
	* No of Records : 200064
	* Size : 10MB
	* File format :Excel
	* Format : Structured

## Data Source Link:
	* https://www.kaggle.com/PromptCloudHQ/hotels-on-makemytrip/data

## Big Data Qualifications / Challenges:
	1. Velocity: We have huge amount of data. And every day lots and many users check out the destinations places so the data incoming would be more as many people make trips and schedule to different visiting place. So speed with which the data is expressed.
	2. Variety: The data, which we choose is structured format. It involves a huge amount of numbers.
	3. Volume: The data, which we choose has many number user who made trips to different place, and they stay at hotels and gave different rating to the hotels.

## Big Data Questions:
	1. For each area, what is maximum rated hotel?(Sangeetha Detne)	
	2. For each City, what are the maximum star rated hotels?(Venkata Naga Kamal Nadh Meduri)
	3. Which city has maximum count of 5 reviews score?(Tilak Dhulipalla)
	4. which city has the highest number of hotels located?(Anil Seshu Kumar Akula)


## Big Data Solutions:
 1. Sangeetha Detne
	
	* Mapper input: Mall Road, Manali, India
	* Mapper Output / Reducer Input: (sorted list of all cities)
	  ![Mapper1 output.png](https://bitbucket.org/repo/x8Gyzgo/images/264166657-Mapper1%20output.png)
	  ![Mapper2.png](https://bitbucket.org/repo/x8Gyzgo/images/3541695721-Mapper2.png)
	* Reducer Output: Udaipur,5
	  ![reducer.png](https://bitbucket.org/repo/x8Gyzgo/images/933019645-reducer.png)
	* Kind of chart: Bar graph, Barchart(different hotel and their rating)
	  ![dispic.PNG](https://bitbucket.org/repo/x8Gyzgo/images/3954128102-dispic.PNG)
	

2. Venkata Naga Kamal Nadh Meduri
	#### Story:
	
		My goal is to find the maximum rated hotels in each city. For that I have taken trimmed and cleared eight columns. In that I choosen three coloums and sorted using alphabetical order and send to Reducer to get maximum rated hotels in each city.
		
	#### Process of execution:
		For this project initially Python has to be installed in the systems. preferably use Visual Studio to open the code folder that is Meduri_MR_Project_Team1-4.
		In that first run Mapper_Meduri.py than run Sort.py finally run Reducer_Meduri.py to get output in final_output.txt file.
		
	* Mapper input: Hardasji Ki Magri, Udaipur,	India, 3 star
	* Mapper Output / Reducer Input : (sorted list of all cities in a country)
	  ![Mapper_op.PNG](https://bitbucket.org/repo/x8Gyzgo/images/822233703-Mapper_op.PNG)
	* Reducer Output: Maximun Star Rated Hotel In Jaisalmer city is :Zion Home
					: Minimum Star Rated Hotel In Jaisalmer city is :Hotel Suman
	  ![Reducer_output.PNG](https://bitbucket.org/repo/x8Gyzgo/images/9349265-Reducer_output.PNG)
	* Kind of chart: Bar or line graph Jaisalmer(different hotel and their rating)
	  ![Graph.PNG](https://bitbucket.org/repo/x8Gyzgo/images/3207237381-Graph.PNG)
	  ![Graph2.PNG](https://bitbucket.org/repo/x8Gyzgo/images/685765686-Graph2.PNG)

3. Tilak Vinuth Nag Dhulipalla
   #### Story:
	
		My goal is to find the count of 5 review score in each city. For that I have taken trimmed and cleared nine columns. In that I choosen two coloums and sorted using alphabetical order and send to Reducer to get the city which has more 5 review score hotels.	
    #### Process of execution:
		For this project initially Python has to be installed in the systems. preferably use Visual Studio to open the code folder that is Dhulipalla_MR_Project_Team1-3.
		In that first run tilak_Mapper.py then run tilak_Reducer.py to get output in finaloutput2.txt file.
	
	* Mapper input :Udaipur, India, 4.5 
	* Mapper Output / Reducer Input : (sorted list of all cities)
	  ![tilak_mapper.JPG](https://bitbucket.org/repo/x8Gyzgo/images/2332517182-tilak_mapper.JPG)
	* Reducer Output :NewDelhi and NCR(17)
	  ![tilak_reducer.JPG](https://bitbucket.org/repo/x8Gyzgo/images/3490168222-tilak_reducer.JPG)
	* Kind of chart : Barchart
	  ![tilak_question3.JPG](https://bitbucket.org/repo/x8Gyzgo/images/4165012639-tilak_question3.JPG)

4. Anil Seshu Kumar Akula
	
	* Mapper input :Udaipur, hotel
	[![mapper.png](https://s9.postimg.cc/cyn38hme7/mapper.png)](https://postimg.cc/image/hxaln0q6z/)
	* Mapper Output / Reducer Input : (sorted list of all cities)
	* Reducer Output : New Delhi(272)
	[![reducer.png](https://s9.postimg.cc/3quurs7m7/reducer.png)](https://postimg.cc/image/pq19ezogb/)
	* Kind of chart : Histogram
	 [![chart.png](https://s9.postimg.cc/8pid6b3pb/chart.png)](https://postimg.cc/image/4t51abipn/)




