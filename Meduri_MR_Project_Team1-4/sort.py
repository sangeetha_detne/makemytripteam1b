before_sorted = open("output.txt", "r")
after_sorted = open("sorted.txt", "w")

data = before_sorted.readlines()
data.sort()

for line in data:
    after_sorted.write(line)


before_sorted.close()
after_sorted.close()